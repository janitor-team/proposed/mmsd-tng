mmsd-tng (1.11)

  [ Chris Talbot ]
  * Fix various memory leaks
  * Improve how service handles requeued messages

mmsd-tng (1.10)

  [ Andrey Skvortsov ]
  * Do not emit certain subjects that are automatically added

  [ Antoine Mercadal ]
  * Fix a couple memory issues

  [ Chris Talbot ]
  * Update Documentation
  * Default to C-ares for DNS

mmsd-tng (1.9)

  [ Antoine Mercadal ]
  * Use DNS settings from ModemManager for C-ares

mmsd-tng (1.8)

  [ Chris Talbot ]
  * Fix various memory leaks
  * Add dbus signal for issues sending/receiving MMSes
  * Work around issue in Modem Manager if it cannot delete SMS WAP
  * Don't make a request for failed decoded MMS

  [ Tim Hollabaugh ]
  * Fix mms proxy issue with AT&T

mmsd-tng (1.7)

  [ Chris Talbot ]
  * Don't try to set modem state
  * Add better network handling for MMS
  * Add support for expired MMSes


  [ Mohammed Sadiq ]
  * Add "ChangeAllSettings" Method Call
  * Refactor changing settings via d-bus

  [ Guido Günther ]
  * Fix logging

mmsd-tng (1.6)

  [ Chris Talbot ]
  * Fix various small bugs

  [ Gled ]
  * Add support for receiving Verizon USA MMS

  [ Guido Günther ]
  * Fix various small bugs

mmsd-tng (1.5)

  [ Chris Talbot ]
  * Reset Settings if IMSI changes
  * Fix Memory Leaks
  * Add Headers to be compatible with VZW USA

  [ Julian Samaroo ]
  * Add frontend CLI

mmsd-tng (1.4)

  [ Chris Talbot ]
  * Fix several memory leaks
  * (I hope) Fix segfault issue when sending MMS
  * Remove stale MMS requests
  * Remove ofono plugin

mmsd-tng (1.3)

  [ Chris Talbot ]
  * Add support for mobile-broadband-provider-info
  * Sync settings to settings file as soon as they are changed

mmsd-tng (1.2)

  [ Chris Talbot ]
  * Depend on libphonenumber instead of libebook-contacts
  * service: filter out content type on sent messages

mmsd-tng (1.1.1)

  [ Chris Talbot ]
  * Initialize auto-pointer to NULL

mmsd-tng (1.1)

  [ Chris Talbot ]
  * Fix various memory leaks
  * Do not display sensitive information in debug logs
  * Work around Android/iOS contact card and calendar bug
  * Fix dbus interface bug
  * Remove depreciated nodaemon option
  * Remove depreciated modemmanager settings code
  * Remove sleep () from code

  [ Travis Wrightsman ]
  * Add option to use systemd-resolved to retrieve

mmsd-tng (1.0)

  [ Chris Talbot ]
  * Fix verious bugs

  [ Evangelos Ribeiro Tzaras ]
  * Fix various memory leaks

mmsd-tng (1.0~beta5)

  [ Chris Talbot ]
  * Add support for delivery reports

  [ Guido Gunther ]
  * Fix various Memory Leaks

mmsd-tng (1.0~beta4)

  [ Chris Talbot ]
  * Fix bug in "ChangeProperty" signal
  * Clean up Copyright Headers

mmsd-tng (1.0~beta3)

  [ Chris Talbot ]
  * Add Modem's own NUmber in "AddMessage" Signal
  * Fix Various issues with libsoup implimentation

  [ Clayton Craft ]
  * Fix Various issues with libsoup implimentation

mmsd-tng (1.0~beta2)

  [ Chris Talbot ]
  * Add option to include Subject in Sent Messages

  [ Clayton Craft ]
  * Replace gweb with libsoup
  * Fix memory leaks

mmsd-tng (1.0~beta1)

  [ Chris Talbot ]
  * Make Maximum Attachments a User Defineable Value
  * Delete Daemon Option
  * Fix Generate UUID for new MMSes
  * Update GetProperties and SetProperty in Modem Manager Plugin
  * Integrate Modem Manager Settings into Main Settings file
  * Format all numbers to E164
  * Add option to create SMIL for MMSes
  * MM: Add feature to choose default modem if you have multiple modems.

mmsd-tng (1.0~beta0) UNRELEASED; urgency=medium

  [ Chris Talbot ]
  * Delete dbus customer functionality
  * Update dbus 1.0 to GIO dbus
  * Disable Ofono Plugin pending update to GIO dbus

mmsd-tng (0.3) UNRELEASED; urgency=medium

  [ Clayton Craft ]
  * Ensure compatibility with muslc

mmsd-tng (0.2) UNRELEASED; urgency=medium

  [ Chris Talbot ]
  * Fix issue if MMS attachment does not have a filename
  * Refactor debug messages
  * Fix intentation of modemmanager.c to match upstream style
  * Add check before g_memdup() to prevent possible buffer overflow

  [ Clayton Craft ]
  * Replace autotools with meson
  * Fix several compiler warnings

mmsd-tng (0.1-4)

  [ Chris T ]
  * Format modemmanager.c to match upstream style
  * Support Autoprocessing SMS WAPs

  [ Clayton Craft ]
  * Replace Autotools with Meson

mmsd-tng (0.1-3) UNRELEASED; urgency=medium

  [ Chris T ]
  * Add ModemManager Support
  * Fix multiple date/time bugs in MMS messages
  * Fix support for Telus Canada

  [ Anteater ]
  * Fix mmsd-tng to work with T-Mobile

  [ Kent ]
  * Add Support for AT&T

  [ Elias Rudberg ]
  * Fix issue if encoded text has length of 0

mmsd (0.0~git20190724-1) UNRELEASED; urgency=medium

  [ Chris T ]
  * Synced with mmsd upstream
